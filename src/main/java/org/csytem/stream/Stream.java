package org.csytem.stream;

import org.csytem.model.Product;
import org.csytem.util.FeedProduct;

import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

public class Stream {
    public static final List<Product> PRODUCTS = FeedProduct.feed();
    public static final List<Integer> NUMBERS = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 7, 7, 7, 8, 9, 10, 12);

    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
        int result = numbers
                .stream()
                .reduce(
                        100, // identity
                        (subtotal, element) -> subtotal + element);// accumulator
        System.out.println("-------");
        // use parallelStream
        List<Integer> listOfNumbers = Arrays.asList(1, 2, 3, 4);
        listOfNumbers.parallelStream().forEach(number ->
                System.out.println(number + " " + Thread.currentThread().getName())
        );

        System.out.println("--------");
        List<Integer> listOfNumberss = Arrays.asList(1, 2, 3, 4);
        int sum = listOfNumberss.parallelStream().reduce(5, Integer::sum); //every thread added five because of this result is 30
        int sum2 = listOfNumbers.parallelStream().reduce(0, Integer::sum) + 5;
        System.out.println(sum);
    }

    public static void foo() {
        long count = PRODUCTS.stream()
                .filter(product -> product.getPrice() > 1000)
                .map(Product::getName)
                .filter(val -> val.contains("1"))
                .count();
        Optional<Integer> reduce = NUMBERS.stream().reduce((a, b) -> a + b);
        System.out.println(NUMBERS.stream().reduce(Integer::sum).get());
        reduce.ifPresent(System.out::println);
        // IntStream.range(0, 100).forEach(i -> System.out.printf("%d", i));
        // IntStream.range(0, 26).forEach(i -> System.out.printf("%c", (char) ('A' + i))) ;
        // System.out.printf("%c", 'a' + 1);

        String str = "merhaba ben buğra celik";

        //tüm karakterler aralarında space karakteri olacak şekilde ekrana basıyor
        IntStream.range(0, str.length())
                .map(str::charAt)
                .forEach(ch -> System.out.printf("%c ", ch));

        System.out.println();

        IntStream.range(0, str.length())
                .map(str::charAt)
                .map(Character::toUpperCase)
                .forEach(ch -> System.out.printf("%c", ch));

        System.out.println();

        IntStream.range(0, str.length())
                .map(str::charAt)
                .filter(ch -> !Character.isWhitespace(ch))
                .forEach(ch -> System.out.printf("%c", ch));

        System.out.println();

        System.out.printf("%2$s %1$s %3$s %1$s %2$s", "merhaba", "ben", "berna");

        System.out.println();

        //faktöriyel hesaplama
        System.out.println(IntStream.range(2, 10).reduce(1, (a, b) -> a * b));
        System.out.println();

        System.out.println(IntStream.rangeClosed(1, 5).reduce((a, b) -> a * b).getAsInt());
        System.out.println();
        //asal sayı bulma
        int prime = 17;
        boolean b = IntStream.rangeClosed(2, prime / 2).noneMatch(i -> prime % i == 0);

        int[] ints = {1, 3, 4, 2, 10};
        //IntStream.of(ints).boxed().forEach(System.out::println);
        //IntStream.of(ints).mapToObj(value -> value * value).forEach(System.out::println);

        IntStream.generate(() -> new Random().nextInt(100)).limit(100).forEach(System.out::println);

    }


}


