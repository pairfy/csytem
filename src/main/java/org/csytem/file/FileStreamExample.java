package org.csytem.file;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.stream.Stream;

public class FileStreamExample {
    public static void main(String[] args) throws IOException {
        Stream<String> lines = Files.lines(Paths.get("deneme.txt.txt"));

       // lines.forEach(System.out::println);

        String str = "merhaba napıyorsun iyi misin?\n";
        String string = "alper eren\n";

        Files.write(Paths.get("bugra.txt"), str.getBytes(StandardCharsets.UTF_8));
        Files.write(Paths.get("bugra.txt"), string.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
        Files.lines(Paths.get("bugra.txt")).forEach(System.out::println);

    }
}
