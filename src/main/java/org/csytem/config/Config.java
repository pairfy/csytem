package org.csytem.config;

import org.csytem.deneme.Armut;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class Config {

    @Bean
    public Armut getArmut() {
        return new Armut("1", "bugra");
    }

    @Bean
    @Primary
    public Armut getOther() {
        return new Armut("2", "4");
    }

}
