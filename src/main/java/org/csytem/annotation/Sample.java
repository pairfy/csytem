package org.csytem.annotation;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
class Sample {
   private String field1;
   private int field2;

   //getters and setters
}