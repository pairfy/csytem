package org.csytem.file;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class DenemeFile {

    static int exceptionCount = 0;
    static long totalUpdatedProduct = 0;

    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File("rapor1");
        Hits hits = objectMapper.readValue(file, Hits.class);
        File file2 = new File("rapor2");
        Hits hits2 = objectMapper.readValue(file2, Hits.class);
        File file3 = new File("rapor3");
        Hits hits3 = objectMapper.readValue(file2, Hits.class);
        File file4 = new File("rapor4");
        Hits hits4 = objectMapper.readValue(file2, Hits.class);
        HashSet<String> accountList = new HashSet<>();
        HashMap<String, Integer> productUpdateMap = new HashMap<>();

        List<Hit> listOfDocs1 = hits.hits;
        List<Hit> listOfDocs2 = hits2.hits;
        List<Hit> listOfDocs3 = hits3.hits;
        List<Hit> listOfDocs4 = hits4.hits;

        listOfDocs1.addAll(listOfDocs2);
        listOfDocs1.addAll(listOfDocs3);
        listOfDocs1.addAll(listOfDocs4);

        for (Hit doc : listOfDocs1) {
            String message = doc.get_source().getMessage();
            int count = messageParser(message);
            String accountName = accountParser(doc.get_source().getLoggerName());
            if (productUpdateMap.containsKey(accountName)) {
                Integer numberOfProduct = productUpdateMap.get(accountName);
                numberOfProduct += count;
                productUpdateMap.put(accountName, numberOfProduct);
            } else {
                productUpdateMap.put(accountName, count);
            }

            accountList.add(accountParser(doc.get_source().getLoggerName()));
            totalUpdatedProduct += count;
        }

        try {
            File rapor = new File("export.txt");
            FileWriter fileWriter = new FileWriter(rapor);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            int i = 1;

            for (Map.Entry<String, Integer> entry : productUpdateMap.entrySet()) {
                String key = entry.getKey();
                Integer value = entry.getValue();

                String val = i++ + "." + "account [" + key + "] update product count last one week : " + value;

                bufferedWriter.write(val);
                bufferedWriter.newLine();
            }
            bufferedWriter.write("total product size : " + totalUpdatedProduct);

            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("exception count : " + exceptionCount);
        System.out.println("total product count : " + totalUpdatedProduct);

    }
    private static String accountParser(String loggerName) {
        int index = loggerName.indexOf("job") + 4;
        int lastIndexOf = loggerName.lastIndexOf(".");
        return loggerName.substring(index, lastIndexOf).trim();
    }

    private static int messageParser(String message) {
        int startIndex = message.lastIndexOf('[') + 1;
        int endIndex = message.lastIndexOf(']');
        String numberStr = message.substring(startIndex, endIndex);
        try {
            return Integer.parseInt(numberStr);
        } catch (Exception e) {
            exceptionCount++;
            return 0;
        }
    }
}
