package org.csytem.postal.model;

import lombok.Data;

@Data
public class MessageLinkClicked extends SegmentifyMessageReceiver {

    private String url;

    @Override
    public String reportStatus() {
       return "Email [clicked]";
    }
}
