package org.csytem.lombokexample;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;

import java.util.logging.Logger;

@Getter
@Setter
@AllArgsConstructor
public class LombokExample {
    @NonNull
    private String name;
    private String surName;
    private String adress;


    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public Double foo() {
        return null;
    }
}
