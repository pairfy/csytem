package org.csytem;

import io.micrometer.core.instrument.util.StringUtils;
import org.csytem.model.RedirectionResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@SpringBootApplication
@RestController
@RequestMapping("hello")
public class CSystemApplication {

    @Value("${cassandra.note.id:localhost}")
    public String[] cassandraNodeId;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(CSystemApplication.class, args);
    }

    @GetMapping("/deneme")
    public void foo321(@RequestBody Map<String, String> sortInfo) {
        System.out.println("y");
    }

    @GetMapping("redirection/{query}")
    public RedirectionResponse denemeJson(@PathVariable String query) {

        if (query.equals("1")) {
            return new RedirectionResponse(true, "www.google.com");
        }

        if (query.equals("2")) {
            return new RedirectionResponse(true, "www.facebook.com");
        }

        return new RedirectionResponse();

    }

    public enum SubReferrer {

        FACEBOOK("Facebook"), INSTAGRAM("Instagram"),
        TWITTER("Twitter"), TICTOK("Tiktok"), SNAPSHOT("Snapchat"),
        OTHER("otherSocialMedia");

        private final String name;

        SubReferrer(String name) {
            this.name = name;
        }
    }

    public enum StatsDevice {
        PC, TABLET, MOBILE, ANDROID, IOS
    }

    private static SubReferrer getSubReferrer(String source) {
        if (StringUtils.isNotEmpty(source)) {
            try {
                return SubReferrer.valueOf(source);
            } catch (IllegalArgumentException e) {
                return SubReferrer.OTHER;
            }
        }
        return SubReferrer.OTHER;
    }
}
