package org.csytem.util;

import org.csytem.model.Product;

import java.util.*;

public class FeedProduct {


    public static List<Product> feed() {
        Random random = new Random();
        List<Product> list = new ArrayList<>();
        for (int i = 1; i < 1000000; i++) {
            Product product = new Product();
            product.setId(i);
            product.setName("name" + i);
            product.setPrice(random.nextInt(200) + random.nextDouble());
            product.setStock(random.nextBoolean());
            list.add(product);
        }
        return list;
    }
    public static Set<Product> feedOfHashSet() {
        Random random = new Random();
        Set<Product> list = new HashSet<>();
        for (int i = 1; i < 1000000; i++) {
            Product product = new Product();
            product.setId(i);
            product.setName("name" + i);
            product.setPrice(random.nextInt(200) + random.nextDouble());
            product.setStock(random.nextBoolean());
            list.add(product);
        }
        return list;
    }

    public static List<Product> feedOfLinkedList() {
        Random random = new Random();
        List<Product> list = new LinkedList<>();
        for (int i = 1; i < 1000000; i++) {
            Product product = new Product();
            product.setId(i);
            product.setName("name" + i);
            product.setPrice(random.nextInt(200) + random.nextDouble());
            product.setStock(random.nextBoolean());
            list.add(product);
        }
        return list;
    }



    public static void main(String[] args) {
        int a = 5;
        int b = 10;
        int sum = a + b;
        System.out.println(sum);

    }


}
