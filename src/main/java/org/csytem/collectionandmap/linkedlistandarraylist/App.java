package org.csytem.collectionandmap.linkedlistandarraylist;

import java.util.ArrayList;
import java.util.LinkedList;

public class App {
    public static void main(String[] args) {
        /*
        LinkedList veri yapısında baglı liste kullanıyor. Datalar birbirlerine sırayla baglı.
        LinkedList'i eger listeye ekleme çıkarma işlemi yapılacaksa tercih edilmesi gerekiyor.
        LinkedList'te listenin 400. elemanını istenildigi zaman 400'e kadar olan elemanları dolaşması gerekiyor.
         */
        LinkedList<String> linkedList = new LinkedList<>();

        linkedList.add("selam");
        linkedList.add("selam");
        linkedList.add("selam");
        linkedList.add("selam");
        linkedList.add("selam");

        /*
        ArrayList arka planda dizi tuttugu için get işlemlerinde tercih edilmeli.
        O(1) de istenilen datayı döner. Ama ekleme ve çıkarma işlemlerinde diziyi koplayıp büyüttügü için
        ekleme ve çıkarme işlemlerinde tercih edilmemelidir.
         */
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("selam");
        arrayList.add("selam");
        arrayList.add("selam");
        arrayList.add("selam");
    }


}
