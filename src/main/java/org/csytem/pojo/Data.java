package org.csytem.pojo;

import lombok.NoArgsConstructor;

import java.util.Map;

@lombok.Data
@NoArgsConstructor
public class Data {
    private String[] url;
    private String version;
    private Map<String, FieldNameDelimeter> fields;
}