package org.csytem.abstarchandinterface;


import lombok.Getter;
import lombok.Setter;
import org.apache.catalina.Service;

import java.io.Serializable;

@Getter
@Setter
//Abstract class doesn't support multiple inheritance.
//An abstract class can extend another Java class and implement multiple Java interfaces.
public abstract class Animal extends Main implements Serializable, Zoo {
    //Abstract class can have final, non-final, static and non-static variables.
    //A Java abstract class can have class members like private, protected, etc.
    public String id;
    private String name;
    protected String channel;
    static String address;
    public final int no;

    Animal() {
        no = 10;
    }

    //abstract class can have abstract and non-abstract methods.
    public abstract String getVoice();
    public abstract String getClassName();

    public abstract String getVoice2();

    //if method will be final you can not override this method in inhereted class.
    public final String getAllAnimalVoice() {
        return "Voice";
    }

    public static String getAnimalName() {
        return "BUGRA";
    }

    public static void main(String[] args) {
        Zoo.myStaticMethod();
    }
}
