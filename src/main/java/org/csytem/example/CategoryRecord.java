package org.csytem.example;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryRecord {
    private Integer categoryId;
}
