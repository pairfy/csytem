package org.csytem.shortcut;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class ShortCutExample {
    public static void main(String[] args) throws IOException {
        // 4. Read in a File
        String fileText = new String(Files.readAllBytes(Paths.get("data.txt")));
        List<String> fileLines = Files.readAllLines(Paths.get("data.txt"));

        // 8. Find minimum (or maximum) in a List
        IntStream.of(14, 35, -7, 46, 98).min();
        Arrays.asList(14, 35, -7, 46, 98).stream().min(Integer::compare);
        Arrays.asList(14, 35, -7, 46, 98).stream().reduce(Integer::min);
        Collections.min(Arrays.asList(14, 35, -7, 46, 98));
    }
}
