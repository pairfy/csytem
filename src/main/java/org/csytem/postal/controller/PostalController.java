package org.csytem.postal.controller;

import org.csytem.postal.model.SegmentifyMessageReceiver;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostalController {

    @RequestMapping(value = {"segmentify-webhook/{accountId}"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> segmentifyWebHook(@PathVariable String accountId, @RequestBody SegmentifyMessageReceiver receiver) {
        try {
            return (ResponseEntity<?>) ResponseEntity.ok();
        } catch (Exception e) {
            return (ResponseEntity<?>) ResponseEntity.badRequest();
        }
    }


}
