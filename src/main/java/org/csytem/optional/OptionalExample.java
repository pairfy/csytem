package org.csytem.optional;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Optional;

public class OptionalExample {
    public static void main(String[] args) {
        Person person = Person.builder().name("Bugra")
                .id(984)
                .surName("Celik")
                .password("324jlJıQF")
                .cardNumber(2385937102343433L)
                .address("Istanbul")
                .build();

        Optional<Person> optionalPerson = Optional.of(person);
        Optional<PersonDto> personDto = optionalPerson.map(val -> new PersonDto(val.getId(), val.getName(), val.getSurName()));
        if (personDto.isPresent()) {
            System.out.println(personDto.get());
        }
    }
}

@Getter
@Setter
@AllArgsConstructor
@Builder
class Person {
    private Integer id;
    private String name;
    private String surName;
    private String password;
    private Long cardNumber;
    private String address;
}

@Getter
@Setter
@AllArgsConstructor
@ToString
class PersonDto {
    private Integer id;
    private String name;
    private String surName;
}
