package org.csytem.postal.model;

import lombok.Data;

@Data
public class MessageLoaded extends SegmentifyMessageReceiver{

    @Override
    public String reportStatus() {
        return "Email [opened]";
    }
}
