package org.csytem.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.UniqueElements;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT) //default values is not showing up
@JsonIgnoreProperties({"name", "birthDate"}) //class level ignore class's variable
public class User {

    @JsonIgnore
    private Integer identityNumber;

    @Size(min = 2, message = "Name should have at least 2 characters")
    @JsonProperty(value = "user_name")
    private String name;

    @Past(message = "Birth date should be in the past")
    private LocalDate birthDate;

    @NotNull(message = "Settings is required!")
    @Size(min = 1, max = 4, message = "DAY, HOUR, MINUTE, SECOND fields are required!")
    @UniqueElements
    private List<ShowOnWebsite> showOnWebsite;

    private static class ShowOnWebsite {

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ShowOnWebsite that = (ShowOnWebsite) o;
            return text == that.text;
        }

        @Override
        public int hashCode() {
            return Objects.hash(text);
        }

        private boolean activate;
        private String text;
    }

}
