package org.csytem.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class Request {
    private List<String> url;
    private String version;
    private Map<String, XmlTag> fields;
}
