package org.csytem.emailvalidator;

import lombok.Data;

import javax.validation.constraints.Email;
import java.util.Set;

@Data
public class Account {
    Set<@Email String> emails;
}
