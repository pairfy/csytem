package org.csytem.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.stream.Stream;

public class FileExample {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("deneme.txt.txt", true);

        String input = "\nhello word";
        String otherInput = "\nbugra celik";

        fileOutputStream.write(input.getBytes());

        fileOutputStream.close();

        FileInputStream fileInputStream = new FileInputStream("deneme.txt.txt");
        int i = 0;

        while ((i = fileInputStream.read()) != -1) {
            System.out.print((char) i);
        }

        System.out.println("************************************************************************");

        //read lines by lines
        Stream<String> lines = Files.lines(Paths.get("deneme.txt.txt"));
        lines.forEach(System.out::println);

        //write paths
        Path write = Files.write(Paths.get("deneme.txt.txt"), otherInput.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);



    }
}
