package org.csytem.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class FieldNameDelimeter {
    private String[] fieldName;
    private String delimeter;
}