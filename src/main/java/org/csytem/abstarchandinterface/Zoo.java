package org.csytem.abstarchandinterface;

import javax.xml.ws.Binding;
import java.io.Serializable;
import java.sql.DriverManager;


//Interface supports multiple inheritance.
//An interface can extend another Java interface only.
public interface Zoo extends Serializable, Binding {

    //Interface can have only abstract methods. Since Java 8, it can have default and static methods also.
    default String getName() {
        return "getName";
    }

    public static String myStaticMethod() { //static method
        return "String";
    }

     static String myStaticMethodd() { //static method
        return "String";
    }

    String getId(); //abstract method

    //has only this field
    //Members of a Java interface are public by default.
    public static final int a = 10;
    int b = 10;


    default void foo() {
        System.out.println("hello");
    }

    default String voo() {
        return "foo";
    }


}
