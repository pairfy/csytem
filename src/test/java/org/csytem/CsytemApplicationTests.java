package org.csytem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.csytem.pojo.Data;
import org.csytem.pojo.Request;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

class CsytemApplicationTests {

    @Test
    void calvinClein() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        String url1 = "https://tr.calvinklein.com/GoogleMC?username=calvinklein&password=calvinklein12345";

        XmlMapper xmlMapper = new XmlMapper();
        URL url = new URL(url1);
        Map<String, Object> map = xmlMapper.readValue(url, Map.class);
        Map<String, Object> channel = (Map<String, Object>) map.get("channel");
        List<Map<String, Object>> item = (List<Map<String, Object>>) channel.get("item");
        System.out.println("h");
    }

    @Test
    void bauhaus() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        String url1 = "https://www.bauhaus.com.tr/google/merchant/all";

        XmlMapper xmlMapper = new XmlMapper();
        URL url = new URL(url1);
        Map<String, Object> map = xmlMapper.readValue(url, Map.class);
        Map<String, Object> channel = (Map<String, Object>) map.get("channel");
        List<Map<String, Object>> item = (List<Map<String, Object>>) channel.get("item");
        System.out.println("h");
    }

    @Test
    void dalkılıc2() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        String url1 = "https://www.dalkilicspor.com/XMLExport/E17F63DF2557472A902E1B69886AA708";

        XmlMapper xmlMapper = new XmlMapper();
        URL url = new URL(url1);
        Map<String, Object> map = xmlMapper.readValue(url, Map.class);
        Map<String, Object> channel = (Map<String, Object>) map.get("channel");
        List<Map<String, Object>> item = (List<Map<String, Object>>) channel.get("item");
        System.out.println("h");
    }

    @Test
    void dalkılıc() throws IOException {
        String url1 = "https://www.josephturner.co.uk/googleshoppingfeed.xml";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", "Application");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML, MediaType.APPLICATION_RSS_XML));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(url1, HttpMethod.GET, entity, String.class);
        String xml = response.getBody();
        /*
        if (xml.charAt(0) == '\uFEFF') {
            xml = xml.substring(1);
        }
         */

        XmlMapper xmlMapper = new XmlMapper();
        Map<String, Object> myPojo = xmlMapper.readValue(xml, Map.class);
        System.out.println("successfully reading");
    }

    @Test
    void josephTurner() throws IOException {
        String url1 = "https://www.josephturner.co.uk/googleshoppingfeed.xml";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", "Application");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML, MediaType.APPLICATION_RSS_XML));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(url1, HttpMethod.GET, entity, String.class);
        String xml = response.getBody();
        /*
        if (xml.charAt(0) == '\uFEFF') {
            xml = xml.substring(1);
        }
         */

        XmlMapper xmlMapper = new XmlMapper();
        Map<String, Object> myPojo = xmlMapper.readValue(xml, Map.class);
        System.out.println("successfully reading");
    }

    @Test
    void denizbutik() throws IOException {
        String url1 = "https://www.choiceandattitude.com/dynamic-remarketing-feed.xml";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", "Application");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML, MediaType.APPLICATION_RSS_XML));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(url1, HttpMethod.GET, entity, String.class);
        String xml = new String(response.getBody().getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        /*
        if (xml.charAt(0) == '\uFEFF') {
            xml = xml.substring(1);
        }
         */

        XmlMapper xmlMapper = new XmlMapper();
        Map<String, Object> myPojo = xmlMapper.readValue(xml, Map.class);
        System.out.println("successfully reading");
    }

    @Test
    void hummel() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        String url1 = "https://www.choiceandattitude.com/dynamic-remarketing-feed.xml";

        XmlMapper xmlMapper = new XmlMapper();
        URL url = new URL(url1);
        Map<String, Object> map = xmlMapper.readValue(url, Map.class);
        Map<String, Object> channel = (Map<String, Object>) map.get("channel");
        List<Map<String, Object>> item = (List<Map<String, Object>>) channel.get("item");
        System.out.println("h");
    }

    @Test
    void convertJson() throws JsonProcessingException {
        String json = "{\n" +
                "  \"url\" : [\"www.google.com\", \"www.segmentify.com\"],\n" +
                "  \"version\" : \"1\",\n" +
                "  \"fields\" : {\n" +
                "      \"productId\" : {\n" +
                "        \"fieldName\" : [\"idtagi\", \"idtagi2\"],\n" +
                "        \"delimeter\" : \">\"\n" +
                "      },\n" +
                "      \"name\" : {\n" +
                "        \"fieldName\" : [\"nametagi\", \"nametagi2\"],\n" +
                "        \"delimeter\" : \",\"\n" +
                "      }\n" +
                "  }\n" +
                "}";

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Data request = objectMapper.readValue(json, Data.class);
        System.out.println("h");

    }

    @Test
    void bigdecimaltest() {
        BigDecimal total = new BigDecimal("23.4242");
        BigDecimal roundedTotal = total.setScale(2, RoundingMode.HALF_EVEN); //sql round fonksiyonuyla aynı mantık
        System.out.println(roundedTotal);

    }

}
