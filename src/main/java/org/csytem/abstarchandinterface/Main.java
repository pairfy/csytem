package org.csytem.abstarchandinterface;

import javax.xml.ws.handler.Handler;
import java.lang.annotation.Annotation;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        //abstract sınıf newleme
        Animal animal = new Animal() {
            @Override
            public List<Handler> getHandlerChain() {
                return null;
            }

            @Override
            public void setHandlerChain(List<Handler> chain) {

            }

            @Override
            public String getBindingID() {
                return null;
            }


            @Override
            public String getVoice() {
                return null;
            }

            @Override
            public String getClassName() {
                return null;
            }

            @Override
            public String getVoice2() {
                return null;
            }
        };
        System.out.println(animal.getNo());


        //interface newleme
        Zoo zoo = new Zoo() {
            @Override
            public List<Handler> getHandlerChain() {
                return null;
            }

            @Override
            public void setHandlerChain(List<Handler> chain) {

            }

            @Override
            public String getBindingID() {
                return null;
            }

            @Override
            public String getId() {
                return null;
            }
        };
    }
}
