package org.csytem.annotation;

import ch.qos.logback.classic.spi.TurboFilterList;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class XMLToPOJO {
   public static void main(String[] args) throws IOException {
      RestTemplate restTemplate = new RestTemplate();
      String url1 =  "https://tr.calvinklein.com/GoogleMC?username=calvinklein&password=calvinklein12345";

      XmlMapper xmlMapper = new XmlMapper();
      URL url = new URL(url1);
      Map<String, Object> map = xmlMapper.readValue(url, Map.class);
      Map<String, Object> channel = (Map<String, Object>) map.get("channel");
      List<Map<String, Object>> item = (List<Map<String, Object>>)channel.get("item");
      System.out.println(map);
   }
}