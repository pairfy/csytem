package org.csytem.model;

import lombok.Data;
import org.csytem.enumcheck.UserOperationsStepValue;

import java.util.Objects;

@Data
public class Product {
    private int id;
    private Double price;
    private String name;
    private boolean stock;
    private UserOperationsStepValue step;

    //YOU CAN NOT OVERRIDE PRIVATE METHOD
    private String foo() {
        return "foo";
    }

    public static String staticMethod() {
        return "Product - staticMethod";
    }

    public String bar () {
        return "bar";
    }

    @Override
    public boolean equals(Object o) {
        Product product = (Product) o;
        return id == product.id;
    }
}
