package org.csytem.example;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Product {
    private List<ProductRecord> productRecordsList = new ArrayList<>();
    private List<CategoryRecord> categoryRecordList = new ArrayList<>();
}
