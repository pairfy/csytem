package org.csytem.abstarchandinterface;

import javax.xml.ws.handler.Handler;
import java.lang.annotation.Annotation;
import java.util.List;

public class Tiger extends Animal{
    @Override
    public String getVoice() {
        return null;
    }

    @Override
    public String getClassName() {
        return null;
    }

    @Override
    public String getVoice2() {
        return null;
    }


    @Override
    public List<Handler> getHandlerChain() {
        return null;
    }

    @Override
    public void setHandlerChain(List<Handler> chain) {

    }

    @Override
    public String getBindingID() {
        return null;
    }
}
