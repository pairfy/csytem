package org.csytem.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Product> productList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            productList.add(foo());
        }


        List<ProductRecord> collect2 = productList.stream().
                flatMap(product -> product.getProductRecordsList().stream()).filter(val -> val.getProductId().equals(1)).collect(Collectors.toList());

        List<CategoryRecord> collect3 = productList.stream().
                flatMap(product -> product.getCategoryRecordList().stream()).filter(val -> val.getCategoryId().equals(1)).collect(Collectors.toList());


        List<ProductRecord> collect1 = productList.stream().map(Product::getProductRecordsList)
                .flatMap(Collection::stream)
                .filter(val -> val.getProductId().equals(1))
                .collect(Collectors.toList());

        List<CategoryRecord> collect = productList.stream().map(Product::getCategoryRecordList)
                .flatMap(Collection::stream)
                .filter(val -> val.getCategoryId().equals(1))
                .collect(Collectors.toList());

        List<ProductRecord> collect4 = productList.stream().flatMap(e -> e.getProductRecordsList().stream()).collect(Collectors.toList());

        


    }

    private static Product foo() {
        Product product = new Product();
        for (int i = 0; i < 10; i++) {
            product.getCategoryRecordList().add(new CategoryRecord(i));
            product.getProductRecordsList().add(new ProductRecord(i));
        }

        return product;
    }
}
