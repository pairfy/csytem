package org.csytem.postal.model;

import lombok.Data;

@Data
public class MessageSent extends SegmentifyMessageReceiver {

    private Long timestamp;
    private String status;
    private String details;

    @Override
    public String reportStatus() {
        return "Email [sent]";
    }

}
