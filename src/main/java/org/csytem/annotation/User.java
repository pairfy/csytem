package org.csytem.annotation;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.internal.org.xml.sax.InputSource;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Component
public class User {

    @Lookup
    public Role createRole() {
        return null;
    };

    public Role assignRole(String name) {
        Role role = createRole();
        role.setName(name);
        return role;
    }

    public static void main(String[] args) {
        try {
            ParameterizedTypeReference<Map<String, String>> responseType = new ParameterizedTypeReference<Map<String, String>>() {};
            String url = "https://tr.calvinklein.com/GoogleMC?username=calvinklein&password=calvinklein12345";
            RestTemplate restTemplate = new RestTemplate();


            String json = restTemplate.getForObject(url, String.class);
            Map<String,Object> map = new HashMap<String,Object>();
            ObjectMapper mapper = new ObjectMapper();

            try {
                //convert JSON string to Map
                map = mapper.readValue(json, new TypeReference<HashMap<String,Object>>(){});
            } catch (Exception e) {
                System.out.println("Exception converting {} to map");
            }


            System.out.println("h");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}