package org.csytem.validation;

import org.csytem.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Deneme {
    public static void main(String[] args) {
        List<As> as = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            as.add(new As(i, "i : " + i));
        }

        List<As> collect = as.stream().filter(val -> val.id % 2 == 0).collect(Collectors.toList());

        collect.forEach(val -> val.id = 10);

        System.out.println("h");
    }


}
