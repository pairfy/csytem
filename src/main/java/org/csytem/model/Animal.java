package org.csytem.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Getter
@Setter
public class Animal {
    private String name;
    private String id;
    private String code;
    private String startDate;
    private String endDate;
    private Set<String> base;
    private Map<String, String> params;

    public Animal() {
    }

    public String speak() {
        return "Animal speak";
    }

    public Animal(String name, String id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", base=" + base +
                ", params=" + params +
                '}';
    }

    public static void main(String[] args) {
        Animal animal = new Animal("bugra1", "1");
        Animal animal1 = new Animal("bugra2", "2");
        Animal animal2 = new Animal("bugra3", "3");
        Animal animal3 = new Animal("bugra4", "4");
        Animal animal4 = new Animal("bugra5", "5");
        Animal animal5 = new Animal("bugra1", "1");
        Animal animal7 = new Animal("aldsad", "12");
        Animal animal77 = new Animal("firstId", "1100");
        Animal animal73 = new Animal("aldsad", "12");
        Animal animal72 = new Animal("aldsad", "12");
        Animal animal74 = new Animal("aldsad", "12");
        Animal animal75 = new Animal("aldsad", "12");
        Animal animal76 = new Animal("aldsad", "13");
        Animal animal778 = new Animal("secondId", "1100");


        List<Animal> animals = Arrays.asList(animal, animal1, animal2, animal3, animal4, animal5, animal7, animal72, animal73, animal74, animal75, animal76, animal77, animal778);

        long begin = System.currentTimeMillis();
        Animal z = animals.stream()
                .filter(val -> val.getId().equals("1100"))
                .findFirst()
                .orElse(null);

        long end = System.currentTimeMillis();
        System.out.println(end - begin + "ms");
        if (z != null) {
            System.out.println(z.getName());
        }

        String name = "";
        long beginCurrentTimeMillis = System.currentTimeMillis();
        for (Animal val : animals) {
            if (val.getId().equals("1100")) {
                name = val.getName();
                break;
            }
        }
        long endCurrentTimeMillis = System.currentTimeMillis();
        System.out.println(endCurrentTimeMillis - beginCurrentTimeMillis + "ms");

        System.out.println(name);

        System.out.println("***********************");

        long beginCurrentTimeMillis2 = System.currentTimeMillis();
        Animal animal6 = find(animals, val -> val.getId().equals("1100"));
        long endCurrentTimeMillis2 = System.currentTimeMillis();
        System.out.println(animal6.getName());
        System.out.println(endCurrentTimeMillis2 - beginCurrentTimeMillis2);


        System.out.println("***********************");
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("1", "1");
        hashMap.put("2", "2");
        hashMap.put("3", "3");
        hashMap.put("4", "4");
        hashMap.put("5", "5");
        hashMap.put("6", "6");
        hashMap.put("7", "7");
        String s = hashMap.get("8");
        System.out.println(s);
    }


     public static <T> T find(List<T> elements, Predicate<T> p) {
        for (T item : elements) {
            if (p.test(item)) {
                return item;
            }
        }
        return null;
    }
}
