package org.csytem.annotation;

import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

public class RESTClient {
  public static void main(String[] args) {

    RestTemplate restTemplate = new RestTemplate();
    String url = "https://tr.calvinklein.com/GoogleMC?username=calvinklein&password=calvinklein12345";

    String result = restTemplate.getForObject(url, String.class);
    try {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(new InputSource(new StringReader(result)));
      System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}





