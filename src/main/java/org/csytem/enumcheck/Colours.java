package org.csytem.enumcheck;

public enum Colours {
    RED(12, 2, 3), BLUE(12, 23, 3), PINK(321, 2, 332), YELLOW(21, 23, 23),
    BLACK(321, 32, 321), WHITE(312, 123, 32), PURPLE(213, 321, 231);

    int x;
    int y;
    int z;

    Colours(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Colours setX() {
        this.x += 10;
        return this;
    }

    public void setY(int y) {
        this.y += 10;
    }

    public void setZ(int z) {
        this.z += 10;
    }
}
