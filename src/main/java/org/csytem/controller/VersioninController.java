package org.csytem.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersioninController {

    @GetMapping("v1/person")
    public void v1Person() {
    }

    @GetMapping("v2/person")
    public void v2Person() {
    }

    @GetMapping(path = "v2/person", produces = "application/vnd.company.app-v1+json")
    public void v2Persoen() {
        //http request header Accept -> application/vnd.company.app-v1+json
    }

    @GetMapping(path = "/stringifier", headers = "X-API-VERSION=1")
    public void init(@RequestParam String str) {
        //http request header X-API-VERSION -> 1
    }

    //request param example with annotation
    @GetMapping(path = "/feed", params = "version=1")
    public void feed(@RequestParam(required = false) Boolean oneMonth) {
        if (oneMonth == Boolean.FALSE) {
            System.out.println("h21");
        }
        System.out.println("hello");
    }
}
