package org.csytem.enumcheck;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.csytem.exceptionhandling.User;

public enum UserOperationsStepValue {
    @JsonProperty("signin")
    SIGN_IN("User Login"),
    @JsonProperty("signup")
    SIGN_UP("User Register"),
    @JsonProperty("signout")
    SING_OUT("User Logout"),
    @JsonProperty("update")
    UPDATE("User Info Update"),
    @JsonProperty("delete_last_search")
    DELETE_LAST_SEARCH("User delete last search history"),
    @JsonProperty("subscribe")
    SUBSCRIBE("User Subscribe Service"),
    @JsonProperty("unsubscribe")
    UNSUBSCRIBE("User Unsubscribe Service");

    private String logName;

    UserOperationsStepValue(String logName) {
        this.logName = logName;
    }

    public String getLogName() {
        return logName;
    }

}
