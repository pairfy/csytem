package org.csytem.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class BasePojo {
    private String productId;
    private String name;
    private String gtin;
    private List<String> additionalImages;
    private List<String> sizes;
}
