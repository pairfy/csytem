package org.csytem.postal.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import java.util.UUID;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "event", include = JsonTypeInfo.As.EXISTING_PROPERTY, visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = MessageSent.class, name = SegmentifyMessageReceiver.EnumStrings.MESSAGE_SENT),
        @JsonSubTypes.Type(value = MessageLinkClicked.class, name = SegmentifyMessageReceiver.EnumStrings.MESSAGE_LINK_CLICKED),
        @JsonSubTypes.Type(value = MessageLoaded.class, name = SegmentifyMessageReceiver.EnumStrings.MESSAGE_LOADED)
})
public abstract class SegmentifyMessageReceiver {

    private String event;
    private UUID uuid;
    private Payload payload;
    private String accountId;

    public abstract String reportStatus();

    @Data
    public static class Payload {
        private Message message;
    }

    @Data
    public static class Message {
        private Integer id;
        private String token;
        private String direction;
        @JsonProperty("message_id")
        private String messageId;
        private String to;
        private String from;
        private String subject;
        private String tag;
    }

    static class EnumStrings {
        public static final String MESSAGE_SENT = "MessageSent"; // when a message is successfully delivered to a recipient/endpoint. Email [sent]
        public static final String MESSAGE_LINK_CLICKED = "MessageLinkClicked"; // event will tell you that a user has clicked on a link in one of your e-mails. Email [clicked]
        public static final String MESSAGE_LOADED = "MessageLoaded"; // event will tell you Email [opened]
    }
}
