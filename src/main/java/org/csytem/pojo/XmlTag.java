package org.csytem.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class XmlTag {
    private List<String> fieldName;
    private String delimeter;
}
