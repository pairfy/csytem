package org.csytem.collectionandmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

public class Main {
    public static void main(String[] args) {


        //feed.stream().forEach(val -> System.out.println(val.getId()));


        //Set Interface ->> Collection<E>
        Set<Integer> treeSet = new TreeSet();
        Set<Integer> linkedHashSet = new LinkedHashSet();
        Set<Integer> hashSet = new HashSet<>();

        //List Interface ->> Collection<E>
        List<String> arrayList = new ArrayList<>();
        List<String> linkedList = new LinkedList<>();
        List<String> vector = new Vector<>();

        //Map Interface
        Map<String, String> treeMap = new TreeMap<>();
        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        Map<String, String> hashMap = new HashMap<>();
        Map<String, String> hashTable = new Hashtable<>();

        hashSet.add(4);
        hashSet.add(1);
        hashSet.add(19);
        hashSet.add(10);
        hashSet.add(11);
        hashSet.add(11);

        treeSet.add(4);
        treeSet.add(1);
        treeSet.add(19);
        treeSet.add(10);
        treeSet.add(11);
        treeSet.add(11);

        linkedHashSet.add(4);
        linkedHashSet.add(1);
        linkedHashSet.add(19);
        linkedHashSet.add(10);
        linkedHashSet.add(11);
        linkedHashSet.add(11);

        System.out.println("HastSet");
        for (Object val : hashSet.toArray()) {
            System.out.println(val);
        }

        System.out.println("******************************************");

        System.out.println("TreeSet");
        for (Object val : treeSet.toArray()) {
            System.out.println(val);
        }

        System.out.println("******************************************");

        System.out.println("LinkedHashSet");
        for (Object val : linkedHashSet.toArray()) {
            System.out.println(val);
        }





    }
}
