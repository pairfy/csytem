package org.csytem.stackandheap;

public class Main {
    public static void main(String[] args) {
        /*
        Bilindiği gibi bütün yerel değişkenler ve metotların parametre değişkenleri belleğin stack bölümünde
        yaratılmaktadır. new operatörüyle yaratılan nesneler belleğin heap denilen bölümünde yaratılırlar.
        Sample s;
        s = new Sample();
        Burada s yerel değişkeni stack'te bulunacaktır. Ancak s’nin gösterdiği yerdeki nesne heap’tedir.
        new operatörü ile tahsis edilen alana sınıf nesnesi (class object) denilmektedir. Sınıf türünden değişkenlere de
        kısaca referans denilmektedir.Bir sınıf nesnesi için bellekte (heap) o sınıfın static olmayan veri elemanlarının toplam uzunluğu kadar yer
        ayrılmaktadır. Sınıfın static veri elemanları ve metotları new ile tahsis edilen alanda yer kaplamaz. Sınıfın static olmayan veri elemanları ardışıl
         bir blok oluşturur. new operatörü bu bloğun başlangıç adresini verir

         */
    }
}
