package org.csytem.date;

import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

@Data
public class DateExample {

    private String name;
    private int id;

    public DateExample(String name, Integer id) {
        if (id == 0) return;
        this.name = name;
        this.id = id;
    }

    public static void main(String[] args) {
        String format = ZonedDateTime
                .now(ZoneId.systemDefault())
                .format(DateTimeFormatter.ofPattern("uuuu.MM.dd.HH.mm.ss"));

        DateExample dateExample1 = new DateExample("bugra", 10);
        DateExample dateExample2 = new DateExample("bugra", 0);

        System.out.println(dateExample1.getId());
        System.out.println(dateExample2.getId());
    }



}



