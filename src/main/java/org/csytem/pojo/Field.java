package org.csytem.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Field {
    private FieldNameDelimeter productId;
    private FieldNameDelimeter name;
}