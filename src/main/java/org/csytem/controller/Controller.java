package org.csytem.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class Controller {

    @GetMapping("hello")
    public String hello() {
        return "hello world";
    }

    @GetMapping("/init")
    public List<String> init(@RequestParam List<String> val) {
        return val;
    }

    @GetMapping("/animal123")
    public void checkEnum321(@RequestParam(required = false) String param) {
        System.out.println(param);
    }

    @GetMapping("servlet")
    public String servlet(HttpServletRequest request) {
        return "ok";
    }



    public enum CampaignStatus {
        ACTIVE, PASSIVE, INTERNAL, ARCHIVED
    }




}

