package org.csytem.controller;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.concurrent.Future;

@Service
public class ExampleService {

    @Async
    public void execute() {
        try {
            Thread.sleep(10000);
            System.out.println(LocalDateTime.now() + " | test finished");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Async
    public Future<String> execute1() {
        try {
            Thread.sleep(10000);
            Future<String> response = new AsyncResult<String>(LocalDateTime.now() + " | test finished");
            System.out.println(response.get());
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
